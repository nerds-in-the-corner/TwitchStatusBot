import os
import sqlite3

import discord
from discord.ext import commands

from dotenv import load_dotenv

# The full path that the scrip gets runned from 
fullpath = os.path.dirname(os.path.realpath(__file__))

# logging
logging.basicConfig(filename='Discordbot.log', filemode='a',
                    format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%d-%b-%y %H:%M:%S', level=os.getenv("LOG_LEVEL"))


# Load dotenv into the environment
load_dotenv()


# Starts up the Discord bot client
client = commands.Bot(command_prefix=os.getenv("PREFIX"),
                      case_insensitive=os.getenv("case_insensitive"))


@client.event
async def on_ready():
    print("Bot is up and running")


@client.command()
async def load(ctx, extension):
    client.load_extension(f"cogs.{extension}")


@client.command()
async def unload(ctx, extension):
    client.unload_extension(f"cogs.{extension}")


@client.command()
@client.has_permissions()
async def reload(ctx):
    exceptions = []
    for filenames in os.listdir(f"{fullpath}/cogs"):
        if filenames.endswith(".py"):
            try:
                client.reload_extension(f"cogs.{filenames[:-3]}")
            except Exception as e:
                exceptions.append(e)
    if not exceptions:
        await ctx.send("All cogs have been successfully been reloaded")
    else:
        await ctx.send(f"one or more errors accured\n {exceptions}")

for filenames in os.listdir(f"{fullpath}/cogs"):
    if filenames.endswith(".py"):
        client.load_extension(f"cogs.{filenames[:-3]}")

if __name__ == "__main__":
    client.run(os.getenv("TOKEN"))
