import os

from sqlalchemy import create_engine, Column, String, Integer, Boolean, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

base_dir = os.path.abspath(os.path.dirname(__file__))
db_path = os.path.join(base_dir, "../db/")
schema = os.path.join(db_path, "schema.sql")

if os.getenv("DATABASE") != ":memory:":
    database = os.path.join(db_path, f"{os.getenv('DATABASE')}.sqlite")
    database = "sqlite:///" + database
    print(database)
else:
    database = "sqlite://"

engine = create_engine(database, echo=True)
session = sessionmaker(bind=engine)
base = declarative_base()


class Streamer(base):
    __tablename__ = "Streamer"

    user_id = Column(Integer, primary_key=True)
    username = Column(String)
    description = Column(String)
    profile_img = Column(String)
    view_count = Column(Integer)

    def __init__(self, user_id, username, description, profile_img, view_count):
        self.user_id = user_id
        self.username = username
        self.description = description
        self.profile_img = profile_img
        self.view_count = view_count

    def create_streamer(self, user_id, username, description, profile_img, view_count):
        if session.query("Streamer").get(user_id):
            print("streamer already exists")
        else:
            new_streamer = Streamer(user_id, username, description, profile_img, view_count)
            session.add(new_streamer)
            session.commit(new_streamer)
            session.close()
    
    def update_streamer(self, user_id, username, description, profile_img, view_count):
        if not session.query("Streamer").get(user_id):
            Print("Streamer not found")
        else:
            session.query("Streamer").filter_by(user_id=user_id).update(username=username, description=description, profile_img=profile_img, view_count=view_count)
            session.commit()
            session.close()

class Active(Base):
    __tablename__ = "active"

    user_id = Column(Integer, ForeignKey(Streamer.user_id), primary_key=True)
    active = Column(Boolean)
    title = Column(String)
    viewer = Column(Integer)
    language = Column(String)
    started_at = Column(DateTime)
    game_id = Column(Integer)

    def __init__(self, user_id, active, title, viewer, language, started_at, game_id):
        self.user_id = user_id
        self.active = active
        self.title = title
        self.viewer = viewer
        self.language = language
        self.started_at = started_at
        self.game_id = game_id

